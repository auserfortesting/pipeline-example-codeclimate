# Analyze Code with Code Climate

An [example script](https://bitbucket.org/ian_buchanan/pipeline-example-codeclimate/src/master/analyze-with-code-climate.bash)
for triggering code analysis in Code Climate from Bitbucket Pipelines.

## How to use it

* Set up the Bitbucket repo for analysis in Code Climate.
* Add environment variables to your Bitbucket Pipelines settings.
* Copy `analyze-with-code-climate.bash` to your project.
* Add `analyze-with-code-climate.bash` to your build configuration.


## Required environment variables

* **`CODE_CLIMATE_API_KEY`**: (Required) API Key for the Code Climate organization.
* **`CODE_CLIMATE_REPO_ID`**: (Required) ID for the Code Climate repository.


## Trigger code analysis

```
curl --fail \
    --data api_token=${CODE_CLIMATE_API_KEY} \
    https://codeclimate.com/api/repos/${CODE_CLIMATE_REPO_ID}/branches/${BITBUCKET_BRANCH}/refresh
```

* Uses `-ex` switches (not shown in snippet above) to exit on first error and to echo command before execution. These help with debugging.
* Uses Curl to make an HTTP POST request with the Code Climate API Token.

## Build configuration

```
pipelines:
  default:
    - step:
        script:
          - pip install -U tox
          - tox
          - ./analyze-with-code-climate.bash
```

* Performs standard build steps for Python.
* Triggers code analysis in Code Climate.
